from django.views.generic import TemplateView
from rest_framework.generics import CreateAPIView, RetrieveAPIView, \
    ListCreateAPIView

from .models import IncomingData, IncomingDataSerializer


class IncomingDataIndexView(TemplateView):
    template_name = 'index.html'


class IncomingDataList(ListCreateAPIView):
    queryset = IncomingData.objects.all()
    serializer_class = IncomingDataSerializer


class IncomingDataAPIView(CreateAPIView, RetrieveAPIView):
    serializer_class = IncomingDataSerializer

