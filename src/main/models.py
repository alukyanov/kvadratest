from rest_framework import serializers

from django.db import models


class IncomingData(models.Model):
    created = models.DateTimeField(auto_now_add=True, blank=True)
    text = models.TextField()

    def to_dict(self):
        return dict(
            id=self.id,
            created=self.created.isoformat(sep=' ')[:19],
            text=self.text
        )


class IncomingDataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    created = serializers.DateTimeField(
        format='%Y-%m-%d %H:%M:%S', required=False)
    text = serializers.CharField()

    def create(self, validated_data):
        return IncomingData.objects.create(**validated_data)
