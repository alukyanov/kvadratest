from django.conf.urls import url

from .views import IncomingDataIndexView, IncomingDataAPIView, IncomingDataList


__all__ = ['urlpatterns']


urlpatterns = [
    url(r'^$', IncomingDataIndexView.as_view(), name='index-page'),
    url(r'^API/list/$', IncomingDataList.as_view(), name='list-text'),
    url(r'^API/uploadText/$', IncomingDataAPIView.as_view(), name='upload-text'),
    url(r'^API/getText/$', IncomingDataAPIView.as_view(), name='get-text'),
]
